# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost1.55-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/securecoin/Securecoin.git /opt/securecoin
RUN cd /opt/securecoin/src && \
    make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost1.55-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r securecoin && useradd -r -m -g securecoin securecoin
RUN mkdir /data
RUN chown securecoin:securecoin /data
COPY --from=build /opt/securecoin/src/securecoind /usr/local/bin/
USER securecoin
VOLUME /data
EXPOSE 12567 12568
CMD ["/usr/local/bin/securecoind", "-datadir=/data", "-conf=/data/securecoin.conf", "-server", "-txindex", "-printtoconsole"]